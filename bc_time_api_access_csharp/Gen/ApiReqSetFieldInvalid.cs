﻿namespace BcTimeApiAccessCsharp.Gen {
    class ApiReqSetFieldInvalid {
        public TimeApiReqStatus Status { get; set; }
        public string[] Data { get; set; }

        public bool HasData {
            get {
                return (this.Data != null && this.Data.Length != 0);
            }
        }
    }
}