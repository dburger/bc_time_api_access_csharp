﻿namespace BcTimeApiAccessCsharp.Gen {
        class ApiReqFail {
            private TimeApiReqStatus g_Status = TimeApiReqStatus.None;

            public TimeApiReqStatus Status {
                get {
                    return this.g_Status;
                }
            }

            public ApiReqFail(TimeApiReqStatus _Status) {
                this.g_Status = _Status;
            }
        }
}
