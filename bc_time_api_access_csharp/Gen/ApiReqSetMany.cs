﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BcTimeApiAccessCsharp.Gen {
    class ApiReqSetMany {
        public TimeApiReqStatus Status { get; set; }
        public List<JObject> Data { get; set; }

        public List<JObject> _Data {
            get {
                return this.Data;
            }
        }

        public bool HasData {
            get {
                return (this.Data != null && this.Data.Count != 0);
            }
        }
    }
}
