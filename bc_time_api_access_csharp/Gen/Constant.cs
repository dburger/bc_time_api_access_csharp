﻿namespace BcTimeApiAccessCsharp.Gen {
    public static class Const {
        public const int CRYPT_LEN_KEY = 32;
        public const int CLIENT_ID_LEN = 30;
        public const int CLIENT_SECRET_LEN = 30;
        public const int CRYPT_LEN_IV = 16;
        public const string CRYPT_IV_START_VALUE = "a,b,c,d,e,f,g,h,A,B,C,D,E,F,G,H"; //a = 1, b = 2..., A = 9, B = 10..., and etc.
        public const string TIME_DOMAIN = "https://time.bcity.me/";
        public const string TIME_HOST = "time.bcity.me";
        public const string TIME_URL_API = "api/";
        public const string TIME_URL_OAUTH2_TOKEN = "oauth2/token/";
        public const string TIME_CONTENT_TYPE = "application/x-www-form-urlencoded";
        public const int TIME_API_UID_GET_ALL = -1;
        public const int TIME_API_UID_POST_MANY = -2;
        public const string JWT_ASSERTION_CONCAT_CHAR = ".";

        //App config values
        public static string APPCONFIG_VAL_CRYPT_KEY = "CRYPT_KEY";
        public static string APPCONFIG_VAL_CLIENT_ID = "CLIENT_ID";
        public static string APPCONFIG_VAL_CLIENT_SECRET = "CLIENT_SECRET";
        public static string APPCONFIG_VAL_PKEY_PATH = "PKEY_PATH";
        public static string APPCONFIG_VAL_GRANT_TYPE = "GRANT_TYPE";
    }
}
