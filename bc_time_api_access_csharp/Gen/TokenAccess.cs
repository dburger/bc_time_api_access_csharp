﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Configuration;
using System.IO;
using System.Security.Cryptography;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading;
using System.Windows.Forms;

namespace BcTimeApiAccessCsharp.Gen {
    public static class TokenAccess {
        private static string g_sClientSecret = String.Empty;
        private static string g_sToken = String.Empty;
        private static DateTime g_dtTokenExpire = DateTime.MinValue;
        private static readonly object g_Lock = new object();
        private const int TRY_MAX = 20;
        private const int THREAD_SLEEP_INTERVAL = 500;
        private static int g_iTry = 0;

        public static OAuth2GrantType GrantType {
            get {
                return (getConfig(ConfigValue.GrantType) == OAuth2GrantType.JwtBearer.ToString()) ? OAuth2GrantType.JwtBearer : OAuth2GrantType.ClientCredential;
            }
            set {
                TokenAccess.setConfig(String.Empty, String.Empty, String.Empty, String.Empty, value.ToString());
            }
        }

        public static string ClientId {
            get {
                return getConfig(ConfigValue.ClientID);
            }
            set {
                TokenAccess.setConfig(value, String.Empty, String.Empty, String.Empty);
            }
        }

        public static string CryptKey {
            get {
                return getConfig(ConfigValue.CryptKey);
            }
            set {
                TokenAccess.setConfig(String.Empty, String.Empty, value, String.Empty);
            }
        }

        public static string Token {
            get {
                return g_sToken;
            }
            set {
                g_sToken = value;
            }
        }

        public static DateTime TokenExpire {
            get {
                return g_dtTokenExpire;
            }
            set {
                g_dtTokenExpire = value;
            }
        }

        public static string KeyPrivateFilePath {
            get {
                return getConfig(ConfigValue.KeyPrivateFilePath);
            }
            set {
                TokenAccess.setConfig(String.Empty, String.Empty, String.Empty, value);
            }
        }

        public static string ClientSecret {
            get {
                if (String.IsNullOrEmpty(CryptKey)) {
                    return getConfig(ConfigValue.ClientSecret);
                }
                if (Crypt.deterEncrypt(getConfig(ConfigValue.ClientSecret), out g_sClientSecret, CryptKey)) {
                    return g_sClientSecret;
                }
                return String.Empty;
            }
            set {
                TokenAccess.setConfig(String.Empty, value, String.Empty, String.Empty);
            }
        }

        public static string getConfig(ConfigValue _Config) {
            string _sValue = String.Empty;
            try {
                Configuration _Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                switch (_Config) {
                    case ConfigValue.ClientID:
                        _sValue = _Configuration.AppSettings.Settings[Const.APPCONFIG_VAL_CLIENT_ID].Value.ToString();
                        return _sValue.Length == Const.CLIENT_ID_LEN ? _sValue : String.Empty;
                    case ConfigValue.ClientSecret:
                        _sValue = _Configuration.AppSettings.Settings[Const.APPCONFIG_VAL_CLIENT_SECRET].Value.ToString();
                        return _sValue.Length == Const.CLIENT_SECRET_LEN ? _sValue : String.Empty;
                    case ConfigValue.CryptKey:
                        _sValue = _Configuration.AppSettings.Settings[Const.APPCONFIG_VAL_CRYPT_KEY].Value.ToString();
                        return _sValue.Length == Const.CRYPT_LEN_KEY ? _sValue : String.Empty;
                    case ConfigValue.KeyPrivateFilePath:
                        _sValue = _Configuration.AppSettings.Settings[Const.APPCONFIG_VAL_PKEY_PATH].Value.ToString();
                        return File.Exists(_sValue) ? _sValue : String.Empty;
                    case ConfigValue.GrantType:
                        return _Configuration.AppSettings.Settings[Const.APPCONFIG_VAL_GRANT_TYPE].Value.ToString();
                }
            } catch (Exception) {
            }
            return String.Empty;
        }

        public static bool setConfig(string _sClientID = "", string _sClientSecret = "", string _sEncryptionKey = "", string _sKeyPrivateFilePath = "", string _sGrantType = "") {
            try {
                Configuration _Configuration = ConfigurationManager.OpenExeConfiguration(Application.ExecutablePath);
                if (getConfig(ConfigValue.ClientID) != String.Empty) {
                    _Configuration.AppSettings.Settings.Remove(Const.APPCONFIG_VAL_CLIENT_ID);
                }
                _Configuration.AppSettings.Settings.Add(Const.APPCONFIG_VAL_CLIENT_ID, _sClientID);
                if (getConfig(ConfigValue.ClientSecret) != String.Empty) {
                    _Configuration.AppSettings.Settings.Remove(Const.APPCONFIG_VAL_CLIENT_SECRET);
                }
                _Configuration.AppSettings.Settings.Add(Const.APPCONFIG_VAL_CLIENT_SECRET, _sClientSecret);
                if (getConfig(ConfigValue.CryptKey) != String.Empty) {
                    _Configuration.AppSettings.Settings.Remove(Const.APPCONFIG_VAL_CRYPT_KEY);
                }
                _Configuration.AppSettings.Settings.Add(Const.APPCONFIG_VAL_CRYPT_KEY, _sEncryptionKey);
                if (getConfig(ConfigValue.KeyPrivateFilePath) != String.Empty) {
                    _Configuration.AppSettings.Settings.Remove(Const.APPCONFIG_VAL_PKEY_PATH);
                }
                _Configuration.AppSettings.Settings.Add(Const.APPCONFIG_VAL_PKEY_PATH, _sKeyPrivateFilePath);
                if (getConfig(ConfigValue.GrantType) != String.Empty) {
                    _Configuration.AppSettings.Settings.Remove(Const.APPCONFIG_VAL_GRANT_TYPE);
                }
                _Configuration.AppSettings.Settings.Add(Const.APPCONFIG_VAL_GRANT_TYPE, _sGrantType);
                _Configuration.Save(ConfigurationSaveMode.Modified);
                return true;
            } catch (Exception) {
            }
            return false;
        }

        public static bool deterToken() {
            try {
                if ( !String.IsNullOrEmpty( g_sToken ) && g_dtTokenExpire > new DateTime() ) {
                    return true;
                }
                if ( Interlocked.Increment( ref g_iTry ) > TRY_MAX ) {
                    Interlocked.Exchange( ref g_iTry, 0 );
                    return false;
                }
                return reqToken();
            } catch ( Exception ) {
            }
            return false;
        }

        private static bool reqToken() {
            string _sCont = "";
            dynamic _ApiReq = null;
            bool _bLockTaken = false;
            try {
                Monitor.TryEnter( g_Lock, ref _bLockTaken );
                if ( !_bLockTaken ) {
                    Thread.Sleep( THREAD_SLEEP_INTERVAL );
                    return deterToken();
                }
                if ( !valid() ) {
                    return false;
                }
                RestRequest _Req = new RestRequest() {
                    Resource = Const.TIME_URL_OAUTH2_TOKEN,
                    Method = Method.POST,
                    RequestFormat = DataFormat.Json
                };
                _Req.AddHeader( "Content-Type", Const.TIME_CONTENT_TYPE );
                _Req.AddParameter( "grant_type", Config.getOAuth2GrantTypeDescr( GrantType ) );
                if ( GrantType == OAuth2GrantType.ClientCredential ) {
                    _Req.AddParameter( "client_id", ClientId );
                    _Req.AddParameter( "client_secret", ClientSecret );
                } else if ( GrantType == OAuth2GrantType.JwtBearer ) {
                    string _sAssertion;
                    if ( deterJwtAssertion( out _sAssertion ) ) {
                        _Req.AddParameter( "assertion", _sAssertion );
                    }
                }
                IRestResponse _Resp;
                if ( !ApiCommHandler.deterResp( _Req, out _Resp ) ) {
                    _ApiReq = new ApiReqFail( _Resp != null && _Resp.StatusCode == 0 ? TimeApiReqStatus.RespNot : TimeApiReqStatus.RespInvalid );
                    return false;
                }
                if ( !String.IsNullOrEmpty( CryptKey ) ) {
                    if ( !Crypt.deterDecrypt( _Resp.Content, out _sCont ) ) {
                        return false;
                    }
                } else {
                    _sCont = _Resp.Content;
                }
                try {
                    _ApiReq = JsonConvert.DeserializeObject( _sCont );
                } catch ( Exception ) {
                }
                if ( _ApiReq != null ) {
                    g_sToken = _ApiReq["access_token"].ToString();
                    g_dtTokenExpire = DateTime.Now.AddSeconds( int.Parse( _ApiReq["expires_in"].ToString() ) );
                    return !String.IsNullOrEmpty( g_sToken ) ? g_dtTokenExpire > new DateTime() : false;
                }
            } catch ( Exception ) {
            } finally {
                if ( _bLockTaken ) {
                    Monitor.Exit( g_Lock );
                    Interlocked.Exchange( ref g_iTry, 0 );
                }
            }
            return false;
        }

        private static bool valid() {
            switch (GrantType) {
                case OAuth2GrantType.ClientCredential:
                    return !String.IsNullOrEmpty(ClientId) && !String.IsNullOrEmpty(ClientSecret);
                case OAuth2GrantType.JwtBearer:
                    return ClientId != String.Empty && KeyPrivateFilePath != String.Empty;
            }
            return false;
        }

        public static bool deterJwtAssertion( out string _sValue, int _iAlgorithm = 1/*PHP constant for SHA1 algorithm*/, string _sCerfiticatePassword = "" ) {
            _sValue = "";
            try {
                UTF8Encoding _UTF8Encode = new UTF8Encoding();
                JObject _JObjAlgo = new JObject() {
                    { "algo", _iAlgorithm},
                };
                DateTime _dtUniversalTime = DateTime.Now.ToUniversalTime();
                JObject _JObjParam = new JObject() {
                    { "client_id", ClientId},
                    { "issued", _dtUniversalTime.ToString("yyyy-MM-dd HH:mm:ss")},
                    { "expire", _dtUniversalTime.AddSeconds(360060).ToString("yyyy-MM-dd HH:mm:ss")}
                };
                string _sSignature;
                string _sAlgoData = Convert.ToBase64String( _UTF8Encode.GetBytes( JsonConvert.SerializeObject( _JObjAlgo ) ) );
                string _sParamData = Convert.ToBase64String( _UTF8Encode.GetBytes( JsonConvert.SerializeObject( _JObjParam ) ) );
                if ( !signData( String.Join( Const.JWT_ASSERTION_CONCAT_CHAR, new string[] { _sAlgoData, _sParamData } ), _sCerfiticatePassword, out _sSignature ) ) {
                    return false;
                }
                _sValue = String.Join( Const.JWT_ASSERTION_CONCAT_CHAR, new string[] { _sAlgoData, _sParamData, _sSignature } );
                return _sAlgoData != String.Empty && _sParamData != String.Empty;
            } catch ( Exception ) {
            }
            return false;
        }

        public static bool signData(string _sDataToSign, string _sCertificatePassword, out string _sSignature) {
            _sSignature = String.Empty;
            try {
                X509Certificate2 _X509Certificate2 = new X509Certificate2(KeyPrivateFilePath, _sCertificatePassword, X509KeyStorageFlags.PersistKeySet);
                RSACryptoServiceProvider _RSACryptoServiceProvider = (RSACryptoServiceProvider)_X509Certificate2.PrivateKey;
                UTF8Encoding _UTF8Encoding = new UTF8Encoding();
                byte[] _byDataToSign = _UTF8Encoding.GetBytes(_sDataToSign);
                byte[] _byArrs = _RSACryptoServiceProvider.SignData(_byDataToSign, new SHA1CryptoServiceProvider()); //SHA1CryptoServiceProvider if _iAlgorithm = 2 (SHA1)
                _sSignature = Convert.ToBase64String(_byArrs);
                if (!verifyData(_sSignature, _sDataToSign, _sCertificatePassword)) {
                    return false;
                }
                return _byArrs != null;
            } catch (CryptographicException) {
            }
            return false;
        }

        public static bool verifyData(string _sSignature, string _sDataToVerify, string _sCertificatePassword) {
            try {
                UTF8Encoding _UTF8Encoding = new UTF8Encoding();
                byte[] _byDataToVerify = _UTF8Encoding.GetBytes(_sDataToVerify);
                byte[] _bySignature = Convert.FromBase64String(_sSignature);
                X509Certificate2 _X509Certificate2 = new X509Certificate2(KeyPrivateFilePath, _sCertificatePassword, X509KeyStorageFlags.PersistKeySet);
                RSACryptoServiceProvider _RSACryptoServiceProvider = (RSACryptoServiceProvider)_X509Certificate2.PrivateKey;
                return _RSACryptoServiceProvider.VerifyData(_byDataToVerify, new SHA1CryptoServiceProvider(), _bySignature);
            } catch (CryptographicException) {
                return false;
            }
        }
    }
}
