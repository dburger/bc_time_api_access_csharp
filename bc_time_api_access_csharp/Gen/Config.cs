﻿using System;

namespace BcTimeApiAccessCsharp.Gen {
    class Config {
        public static string getOAuth2GrantTypeDescr(OAuth2GrantType _OAuth2GrantType) {
            try {
                switch (_OAuth2GrantType) {
                    case OAuth2GrantType.AuthCode:
                        return "authorisation_code";
                    case OAuth2GrantType.Implicit:
                        return "implicit";
                    case OAuth2GrantType.UsrCredential:
                        return "password";
                    case OAuth2GrantType.ClientCredential:
                        return "client_credentials";
                    case OAuth2GrantType.TokenRefresh:
                        return "refresh_token";
                    case OAuth2GrantType.JwtBearer:
                        return "urn:ietf:params:oauth:grant-type:jwt-bearer";
                }
            } catch (Exception) {
            }
            return String.Empty;
        }
    }
}