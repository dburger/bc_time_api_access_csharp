﻿using Newtonsoft.Json.Linq;
using System.Collections.Generic;

namespace BcTimeApiAccessCsharp.Gen {
    class ApiReqGetOne {
        public TimeApiReqStatus Status { get; set; }
        public JObject Data { get; set; }

        public List<JObject> _Data {
            get {
                return new List<JObject>() { Data };
            }
        }

        public bool HasData {
            get {
                return (this.Data != null);
            }
        }
    }
}
