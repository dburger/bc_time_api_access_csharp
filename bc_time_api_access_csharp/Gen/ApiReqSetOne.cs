﻿using Newtonsoft.Json.Linq;

namespace BcTimeApiAccessCsharp.Gen {
    class ApiReqSetOne {
        public TimeApiReqStatus Status { get; set; }
        public JObject Data { get; set; }

        public bool HasData {
            get {
                return (this.Data != null);
            }
        }
    }
}
