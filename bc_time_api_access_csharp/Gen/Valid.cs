﻿using System;

namespace BcTimeApiAccessCsharp.Gen {
    public class Valid {
        public static bool isBetween(int _iValue, int _iMin, int _iMax, bool _bIncl = true) {
            try {
                return (
                    _bIncl
                    ? _iValue >= _iMin && _iValue <= _iMax
                    : _iValue > _iMin && _iValue < _iMax
                );
            } catch (Exception) {
            }
            return false;
        }
    }
}
