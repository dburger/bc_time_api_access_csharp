﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading;

namespace BcTimeApiAccessCsharp.Gen {
    class ApiCommHandler {
        public ApiCommHandler () {
        }

        private bool deterGetReqData(ContentType _ContType, JObject _KeyNValues, ref RestRequest _Req, uint[] _iContUids, int _iPage = 0, int _iRowCount = 0) {
            try {
                if ( !TokenAccess.deterToken() ) {
                    return false;
                }
                JObject _JObj = new JObject() {
                    { "cont_type_id", ((int)_ContType).ToString() },
                    { "cont_uid", _iContUids == null || _iContUids.Length == 0 ? Const.TIME_API_UID_GET_ALL.ToString() :  String.Join(",", _iContUids)},
                };
                if (_iPage > 0 && _iRowCount > 0) {
                    _JObj["page"] = _iPage;
                    _JObj["row_count"] = _iRowCount;
                }
                if (_KeyNValues != null) {
                    foreach (var _Index in _KeyNValues) {
                        _JObj[_Index.Key] = _Index.Value;
                    }
                }
                _Req.AddParameter("access_token", TokenAccess.Token);
                if (String.IsNullOrEmpty( TokenAccess.CryptKey)) {
                    foreach (var _Index in _JObj) {
                        _Req.AddParameter(_Index.Key, _Index.Value);
                    }
                } else {
                    string _sData;
                    if (!Crypt.deterEncrypt(JsonConvert.SerializeObject(_JObj), out _sData)) {
                        return false;
                    }
                    _Req.AddParameter("data", _sData);
                }
                return _Req != null;
            } catch(Exception) {
            }
            return false;
        }

        private bool deterSetReqData( ContentType _ContType, SetReqType _SetReqType, JArray _JArray, ref RestRequest _Req ) {
            string _sData = "";
            try {
                if ( !TokenAccess.deterToken() ) {
                    return false;
                }
                JObject _JObject = new JObject();
                _Req.AddParameter( "access_token", TokenAccess.Token );
                if ( _SetReqType == SetReqType.SetOne ) {
                    _JObject = _JArray.ToObject<List<JObject>>().FirstOrDefault();
                    _JObject["cont_type_id"] = ( (int)_ContType ).ToString();
                    if ( String.IsNullOrEmpty( TokenAccess.CryptKey ) ) {
                        foreach ( var _Index in _JObject ) {
                            _Req.AddParameter( _Index.Key, _Index.Value );
                        }
                    } else if ( !Crypt.deterEncrypt( JsonConvert.SerializeObject( _JObject ), out _sData ) ) {
                        return false;
                    }
                    _Req.AddParameter( "data", _sData );
                    return _Req != null;
                }
                _JObject["cont_type_id"] = ( (int)_ContType ).ToString();
                _JObject["cont_uid"] = Const.TIME_API_UID_POST_MANY;
                _JObject["data"] = _JArray;
                if ( String.IsNullOrEmpty( TokenAccess.CryptKey ) ) {
                    _sData = JsonConvert.SerializeObject( _JObject );
                } else if ( !Crypt.deterEncrypt( JsonConvert.SerializeObject( _JObject ), out _sData ) ) {
                    return false;
                }
                _Req.AddParameter( "data", _sData );
                return _Req != null;
            } catch ( Exception ) {
            }
            return false;
        }

        private bool deterRespData(IRestResponse _Resp, out string _sCont) {
            _sCont = "";
            try {
                if (!String.IsNullOrEmpty( TokenAccess.CryptKey)) {
                    if (!Crypt.deterDecrypt(_Resp.Content, out _sCont)) {
                        return false;
                    }
                } else {
                    _sCont = _Resp.Content;
                }
                return true;
            } catch (Exception) {
            }
            return false;
        }

        public bool getData(ContentType _ContType, out dynamic _ApiReq, JObject _KeyNValues = null, uint[] _iContUids = null, int _iPage = 0, int _iRowCount = 0) {
            string _sCont = "";
            _ApiReq = null;
            try {
                RestRequest _Req = new RestRequest() {
                    Resource = Const.TIME_URL_API,
                    Method = Method.GET,
                    RequestFormat = DataFormat.Json
                };
                _Req.AddHeader("Host", Const.TIME_HOST);
                _Req.AddHeader("Content-Type", Const.TIME_CONTENT_TYPE);
                this.deterGetReqData(_ContType, _KeyNValues, ref _Req, _iContUids, _iPage, _iRowCount);
                IRestResponse _Resp;
                if (!deterResp(_Req, out _Resp)) {
                    _ApiReq = new ApiReqFail(_Resp != null && _Resp.StatusCode == 0 ? TimeApiReqStatus.RespNot : TimeApiReqStatus.RespInvalid);
                    return false;
                }
                if (!deterRespData(_Resp, out _sCont)) {
                    return false;
                }
                try {
                    _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReqGetOne));
                } catch {
                    _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReqGetMany));
                }
                if (_ApiReq != null) {
                    return (_ApiReq.Status == TimeApiReqStatus.Success);
                }
            } catch (Exception) {
            } finally {
                if (_ApiReq == null) {
                    _ApiReq = new ApiReqFail(TimeApiReqStatus.RespJsonInvalid);
                }
            }
            return false;
        }

        public bool setData( ContentType _ContType, SetReqType _SetReqType, JArray _JArray, out dynamic _ApiReq) {
            string _sCont;
            _ApiReq = null;
            try {
                RestRequest _Req = new RestRequest() {
                    Resource = Const.TIME_URL_API,
                    Method = Method.POST,
                    RequestFormat = DataFormat.Json
                };
                _Req.AddHeader("Host", Const.TIME_HOST);
                _Req.AddHeader("Content-Type", Const.TIME_CONTENT_TYPE);
                this.deterSetReqData( _ContType, _SetReqType, _JArray, ref _Req );
                IRestResponse _Resp;
                if (!deterResp(_Req, out _Resp)) {
                    _ApiReq = new ApiReqFail(_Resp != null && _Resp.StatusCode == 0 ? TimeApiReqStatus.RespNot : TimeApiReqStatus.RespInvalid);
                    return false;
                }
                if (!deterRespData(_Resp, out _sCont)) {
                    return false;
                }
                try {
                    _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReq));
                } catch {
                    return false;
                }
                if (_ApiReq == null) { return false; }
                if (_ApiReq.Status != TimeApiReqStatus.FieldInvalid) {
                    try {
                        _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReqSetOne));
                    } catch {
                        _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReqSetMany));
                    }
                } else {
                    _ApiReq = JsonConvert.DeserializeObject(_sCont, typeof(ApiReqSetFieldInvalid));
                }
                if (_ApiReq != null) {
                    return (_ApiReq.Status == TimeApiReqStatus.Success);
                }
            } catch (Exception) {
            } finally {
                if (_ApiReq == null) {
                    _ApiReq = new ApiReqFail(TimeApiReqStatus.RespJsonInvalid);
                }
            }
            return false;
        }

        public static bool deterResp(RestRequest _Req, out IRestResponse _Resp, int _iMillisecWait = 1000, int _iMaxTry = 3) {
            _Resp = null;
            try {
                for (int _iTry = 0; _iTry < _iMaxTry; ++_iTry) { //effectively, we try 3 times
                    RestClient _RestClient = new RestClient(Const.TIME_DOMAIN);
                    _Resp = _RestClient.Execute(_Req);
                    if (_Resp.StatusCode == HttpStatusCode.OK) { return true; }
                    if (_Resp.StatusCode != 0) { break; }
                    if (_iMillisecWait > 0) { Thread.Sleep(_iMillisecWait); }
                }
            } catch (Exception) {
            }
            return false;
        }
    }
}
