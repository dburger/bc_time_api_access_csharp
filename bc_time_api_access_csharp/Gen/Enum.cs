﻿namespace BcTimeApiAccessCsharp.Gen {
    public enum TimeApiReqStatus {
        None = 0,
        Success = 1,
        DataInvalid = 2,
        InsufficientAccess = 3,
        Err = 4,
        ConsumerKeyInvalid = 5,
        ConsumerSecretInvalid = 6,
        MethodInvalid = 7,
        ContTypeIdInvalid = 8,
        UidInvalid = 9,
        FieldInvalid = 10,
        UsrUidInvalid = 11,
        PostBatchDataInvalid = 12,
        DataEmpty = 13,
        RespNot = 14, //no internet connection, blocked by firewall, etc.
        RespInvalid = 15, //http code is anything other than 200 (OK)
        RespJsonInvalid = 16,
        CompProfActiveNot = 17,
        CostPlanOverdue = 18,
        TokenAccessInvalid = 19,
        TokenAccessExpired = 20,
        AuthCredentialInvalid = 21
    }

    public enum ContentType {
        None = 0,
        Device = 18,
        DeviceTimeZone = 54,
        GrpTimeZone = 58,
        CommCmd = 77,
        Emp = 22,
        EmpTmpl = 25,
        AttendRaw = 100,
        ApiAuth = 73
    }

    public enum OAuth2GrantType {
        AuthCode,
        Implicit,
        UsrCredential,
        ClientCredential,
        TokenRefresh,
        JwtBearer
    }

    public enum Status {
        Active = 1,
        Inactive = 2
    }

    public enum SetReqType {
        SetOne = 1,
        SetMany = 2
    }

    public enum ConfigValue {
        ClientSecret,
        ClientID,
        CryptKey,
        KeyPrivateFilePath,
        GrantType
    }
}
