﻿using System;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;

namespace BcTimeApiAccessCsharp.Gen {
    public class Crypt {
        private static string encrypt(string _sValue, string _sKey, string _sIv) {
            try {
                using (RijndaelManaged _Rijndael = new RijndaelManaged() {
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 128
                }) {
                    byte[] _chEncrypt = Encoding.UTF8.GetBytes(_sValue);
                    if (_sKey.Length != Const.CRYPT_LEN_KEY) {
                        return string.Empty;
                    }
                    if (_sIv.Length != Const.CRYPT_LEN_IV) {
                        return string.Empty;
                    }
                    using (ICryptoTransform _Encryptor = _Rijndael.CreateEncryptor(Encoding.UTF8.GetBytes(_sKey), Encoding.UTF8.GetBytes(_sIv)))
                    using (MemoryStream _Stream = new MemoryStream())
                    using (CryptoStream _CryptoStream = new CryptoStream(_Stream, _Encryptor, CryptoStreamMode.Write)) {
                        _CryptoStream.Write(_chEncrypt, 0, _chEncrypt.Length);
                        _CryptoStream.FlushFinalBlock();
                        return Convert.ToBase64String(_Stream.ToArray());
                    }
                }
            } catch (Exception) {
            }
            return string.Empty;
        }

        public static bool deterEncrypt(string _sValue, out string _sEncrypted, string _sKey = "") {
            string _sIv, _sStart, _sEnd;
            _sEncrypted = string.Empty;
            try {
                if (!deterIvStartEnd(out _sIv, out _sStart, out _sEnd)) { return false; }
                if (_sKey == String.Empty) {
                    _sKey = TokenAccess.getConfig(ConfigValue.CryptKey);
                }
                _sEncrypted = encrypt(_sValue, _sKey, _sIv);
                if (_sEncrypted != String.Empty) {
                    int _iCount = 0;
                    while (_sEncrypted.Substring(_sEncrypted.Length - 1) == "=") {
                        _sEncrypted = _sEncrypted.Substring(0, _sEncrypted.Length - 1);
                        ++_iCount;
                    }
                    _sEncrypted = _sStart + _sEncrypted + _sEnd;
                    for (int _iI = 0; _iI < _iCount; _iI++) {
                        _sEncrypted += "=";
                    }
                    return true;
                }
            } catch (Exception) {
            }
            return false;
        }

        private static string decrypt(string _sValue, string _sKey, string _sIv) {
            try {
                using (RijndaelManaged _Rijndael = new RijndaelManaged() {
                    Mode = CipherMode.CBC,
                    KeySize = 256,
                    BlockSize = 128
                }) {
                    byte[] _chValue = Convert.FromBase64String(_sValue),
                             _chDecrypt = new byte[_chValue.Length];
                    if (_sKey.Length != Const.CRYPT_LEN_KEY) {
                        return string.Empty;
                    }
                    if (_sIv.Length != Const.CRYPT_LEN_IV) {
                        return string.Empty;
                    }
                    using (ICryptoTransform _Decryptor = _Rijndael.CreateDecryptor(Encoding.UTF8.GetBytes(_sKey), Encoding.UTF8.GetBytes(_sIv)))
                    using (MemoryStream _Stream = new MemoryStream(_chValue))
                    using (CryptoStream _CryptoStream = new CryptoStream(_Stream, _Decryptor, CryptoStreamMode.Read)) {
                        _CryptoStream.Read(_chDecrypt, 0, _chDecrypt.Length);
                        return Encoding.UTF8.GetString(_chDecrypt);
                    }
                }
            } catch (Exception) {
            }
            return string.Empty;
        }

        public static bool deterDecrypt(string _sValue, out string _sDecrypted, string _sKey = "", bool _bTrimPaddingChar = false) {
            string _sIv;
            _sDecrypted = string.Empty;
            try {
                if (!extractIv(ref _sValue, out _sIv)) { return false; }
                if (_sKey == String.Empty) { _sKey = TokenAccess.getConfig(ConfigValue.CryptKey); }
                _sDecrypted = decrypt(_sValue, _sKey, _sIv);
                if (_bTrimPaddingChar) {
                    _sDecrypted = _sDecrypted.TrimEnd(new char[] { '\0' });
                }
                return _sDecrypted != String.Empty;
            } catch (Exception) {
            }
            return false;
        }

        private static bool deterIv(out string _sValue) {
            _sValue = string.Empty;
            try {
                using (SymmetricAlgorithm _SymAlgorithm = SymmetricAlgorithm.Create()) {
                    _SymAlgorithm.GenerateIV();
                    _sValue = Convert.ToBase64String(_SymAlgorithm.IV).Substring(0, Const.CRYPT_LEN_IV);
                    return (_sValue.Length == Const.CRYPT_LEN_IV);
                }
            } catch (Exception) {
            }
            return false;
        }

        private static bool extractIv(ref string _sEncryped, out string _sValue) {
            int _iLenStart, _iLenEnd, _iCount = 0;
            _sValue = string.Empty;
            try {
                if (_sEncryped.Length <= (Const.CRYPT_LEN_IV + 1)) {
                    return false;
                }
                if (!deterExtractIvLenStart(ref _sEncryped, out _iLenStart)) {
                    return false;
                }
                while (_sEncryped.Substring(_sEncryped.Length - 1) == "=") {
                    _sEncryped = _sEncryped.Substring(0, _sEncryped.Length - 1);
                    ++_iCount;
                }
                _iLenEnd = (Const.CRYPT_LEN_IV - _iLenStart);
                _sValue = _sEncryped.Substring(0, _iLenStart); //start
                _sEncryped = _sEncryped.Substring(_iLenStart);
                _sValue += _sEncryped.Substring(_sEncryped.Length - _iLenEnd); //end
                _sEncryped = _sEncryped.Substring(0, _sEncryped.Length - _iLenEnd);
                for (int _iI = 0; _iI < _iCount; _iI++) {
                    _sEncryped += "=";
                }
                return (_sValue.Length == Const.CRYPT_LEN_IV);
            } catch (Exception) {
            }
            return false;
        }

        private static bool deterExtractIvLenStart(ref string _sEncrypted, out int _iValue) {
            _iValue = -1;
            try {
                if (_sEncrypted.Trim() == String.Empty) { return false; }
                string[] _sValues = Const.CRYPT_IV_START_VALUE.Split(',');
                _iValue = Array.IndexOf(_sValues, _sEncrypted.Substring(0, 1));
                if (_iValue >= 0) {
                    _sEncrypted = _sEncrypted.Substring(1);
                    _iValue += 1;
                    return Valid.isBetween(_iValue, 1, Const.CRYPT_LEN_IV); //zero based
                }
            } catch (Exception) {
            }
            return false;
        }

        private static bool deterIvStartEnd(out string _sIv, out string _sStart, out string _sEnd) {
            int _iLenStart = 0;
            _sIv = string.Empty;
            _sStart = string.Empty;
            _sEnd = string.Empty;
            try {
                if (!deterIv(out _sIv)) { return false; }
                string[] _sValues = Const.CRYPT_IV_START_VALUE.Split(',');
                _iLenStart = (new Random()).Next(1, _sValues.Length);
                _sStart = _sValues[_iLenStart - 1] + _sIv.Substring(0, _iLenStart);
                _sEnd = _sIv.Substring(_iLenStart);
                return ((_sStart.Length + _sEnd.Length) == (Const.CRYPT_LEN_IV + 1));
            } catch (Exception) {
            }
            return false;
        }

        public static bool deterCryptKeyUseClientId(string _sClientId, out string _sValue, int _iKeyLen = 32, int _iLenClientId = 30) {
            _sValue = string.Empty;
            try {
                if (_iKeyLen < _iLenClientId) {
                    return false;
                } else if (_sClientId.Length != _iLenClientId) {
                    return false;
                }
                int[] _iLenParts = new int[5] { 6, 5, 4, 9, 8 };
                if (_iLenParts.Sum() != _iKeyLen) {
                    return false;
                }
                string _sBuff;
                for (int _iI = 0; _iI < _iLenParts.Length; _iI++) {
                    _sClientId = strRev(_sClientId);
                    _sBuff = _sClientId.Substring(0, _iLenParts[_iI]);
                    _sValue += (
                        (_iI % 2) == 0
                        ? _sBuff.Substring(0, 2).ToUpper() + _sBuff.Substring(2)
                        : _sBuff.Substring(0, 1).ToLower() + _sBuff.Substring(1)
                    );
                }
                _sClientId = strRev(_sClientId);
                int _iProg = 0;
                while (_sValue.Length != _iKeyLen) {
                    _sValue += _sClientId.Substring(_iProg++, 1);
                }
                string[] _sChars = "a,d,G,k,M,r,T,u,w,Y".Split(',');
                for (int _iI = 0; _iI < _sChars.Length; _iI++) {
                    _sValue = _sValue.Replace(_sChars[_iI], _iI.ToString());
                }
                return (_sValue.Length == _iKeyLen);
            } catch (Exception) {
            }
            return false;
        }

        public static string strRev(string _sValue) {
            try {
                if (_sValue.Trim() == String.Empty) {
                    return _sValue;
                }
                char[] _chValues = _sValue.ToCharArray();
                Array.Reverse(_chValues);
                _sValue = new string(_chValues);
            } catch (Exception) {
            }
            return _sValue;
        }
    }
}