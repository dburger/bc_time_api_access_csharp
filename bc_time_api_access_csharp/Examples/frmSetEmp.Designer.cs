﻿namespace BcTimeApiAccessCsharp.Examples {
    partial class frmSetEmp {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing) {
            if (disposing && (components != null)) {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent() {
            this.txtName = new System.Windows.Forms.TextBox();
            this.button1 = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSurname = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.txtPayrollNo = new System.Windows.Forms.TextBox();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.numBranch = new System.Windows.Forms.NumericUpDown();
            this.label5 = new System.Windows.Forms.Label();
            this.numDept = new System.Windows.Forms.NumericUpDown();
            this.label6 = new System.Windows.Forms.Label();
            this.numUID = new System.Windows.Forms.NumericUpDown();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.button2 = new System.Windows.Forms.Button();
            this.numUID2 = new System.Windows.Forms.NumericUpDown();
            this.numDept2 = new System.Windows.Forms.NumericUpDown();
            this.txtName2 = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.txtSurname2 = new System.Windows.Forms.TextBox();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.numBranch2 = new System.Windows.Forms.NumericUpDown();
            this.txtPayrollNo2 = new System.Windows.Forms.TextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.button3 = new System.Windows.Forms.Button();
            this.label15 = new System.Windows.Forms.Label();
            this.cmbDevPriv = new System.Windows.Forms.ComboBox();
            this.cmbDevPriv2 = new System.Windows.Forms.ComboBox();
            ((System.ComponentModel.ISupportInitialize)(this.numBranch)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDept)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUID)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUID2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDept2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBranch2)).BeginInit();
            this.SuspendLayout();
            // 
            // txtName
            // 
            this.txtName.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName.Location = new System.Drawing.Point(118, 45);
            this.txtName.Name = "txtName";
            this.txtName.Size = new System.Drawing.Size(219, 21);
            this.txtName.TabIndex = 5;
            // 
            // button1
            // 
            this.button1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Location = new System.Drawing.Point(296, 164);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(155, 27);
            this.button1.TabIndex = 3;
            this.button1.Text = "Save/Update Employee 1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(13, 48);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(44, 15);
            this.label1.TabIndex = 6;
            this.label1.Text = "Name:";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(13, 74);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 8;
            this.label2.Text = "Surname:";
            // 
            // txtSurname
            // 
            this.txtSurname.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSurname.Location = new System.Drawing.Point(118, 71);
            this.txtSurname.Name = "txtSurname";
            this.txtSurname.Size = new System.Drawing.Size(219, 21);
            this.txtSurname.TabIndex = 7;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(13, 101);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(67, 15);
            this.label3.TabIndex = 10;
            this.label3.Text = "Payroll no.:";
            // 
            // txtPayrollNo
            // 
            this.txtPayrollNo.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayrollNo.Location = new System.Drawing.Point(118, 97);
            this.txtPayrollNo.Name = "txtPayrollNo";
            this.txtPayrollNo.Size = new System.Drawing.Size(219, 21);
            this.txtPayrollNo.TabIndex = 9;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(13, 22);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(31, 15);
            this.label7.TabIndex = 18;
            this.label7.Text = "UID:";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(13, 127);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(49, 15);
            this.label4.TabIndex = 19;
            this.label4.Text = "Branch:";
            // 
            // numBranch
            // 
            this.numBranch.Font = new System.Drawing.Font("Arial", 9F);
            this.numBranch.Location = new System.Drawing.Point(118, 122);
            this.numBranch.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numBranch.Name = "numBranch";
            this.numBranch.Size = new System.Drawing.Size(62, 21);
            this.numBranch.TabIndex = 20;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(13, 176);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(96, 15);
            this.label5.TabIndex = 21;
            this.label5.Text = "Device privilege:";
            // 
            // numDept
            // 
            this.numDept.Font = new System.Drawing.Font("Arial", 9F);
            this.numDept.Location = new System.Drawing.Point(118, 148);
            this.numDept.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numDept.Name = "numDept";
            this.numDept.Size = new System.Drawing.Size(62, 21);
            this.numDept.TabIndex = 24;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(13, 151);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(75, 15);
            this.label6.TabIndex = 23;
            this.label6.Text = "Department:";
            // 
            // numUID
            // 
            this.numUID.Font = new System.Drawing.Font("Arial", 9F);
            this.numUID.Location = new System.Drawing.Point(118, 20);
            this.numUID.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numUID.Name = "numUID";
            this.numUID.Size = new System.Drawing.Size(62, 21);
            this.numUID.TabIndex = 25;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.cmbDevPriv);
            this.groupBox1.Controls.Add(this.numUID);
            this.groupBox1.Controls.Add(this.button1);
            this.groupBox1.Controls.Add(this.numDept);
            this.groupBox1.Controls.Add(this.txtName);
            this.groupBox1.Controls.Add(this.label6);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.txtSurname);
            this.groupBox1.Controls.Add(this.label5);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.numBranch);
            this.groupBox1.Controls.Add(this.txtPayrollNo);
            this.groupBox1.Controls.Add(this.label4);
            this.groupBox1.Controls.Add(this.label3);
            this.groupBox1.Controls.Add(this.label7);
            this.groupBox1.Font = new System.Drawing.Font("Arial", 9F);
            this.groupBox1.Location = new System.Drawing.Point(10, 59);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(457, 202);
            this.groupBox1.TabIndex = 26;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Employee 1";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.cmbDevPriv2);
            this.groupBox2.Controls.Add(this.button2);
            this.groupBox2.Controls.Add(this.numUID2);
            this.groupBox2.Controls.Add(this.numDept2);
            this.groupBox2.Controls.Add(this.txtName2);
            this.groupBox2.Controls.Add(this.label8);
            this.groupBox2.Controls.Add(this.label9);
            this.groupBox2.Controls.Add(this.txtSurname2);
            this.groupBox2.Controls.Add(this.label10);
            this.groupBox2.Controls.Add(this.label11);
            this.groupBox2.Controls.Add(this.numBranch2);
            this.groupBox2.Controls.Add(this.txtPayrollNo2);
            this.groupBox2.Controls.Add(this.label12);
            this.groupBox2.Controls.Add(this.label13);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Font = new System.Drawing.Font("Arial", 9F);
            this.groupBox2.Location = new System.Drawing.Point(10, 283);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(457, 201);
            this.groupBox2.TabIndex = 27;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Employee 2";
            // 
            // button2
            // 
            this.button2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Location = new System.Drawing.Point(296, 164);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(155, 27);
            this.button2.TabIndex = 26;
            this.button2.Text = "Save/Update Employee 2";
            this.button2.UseVisualStyleBackColor = true;
            this.button2.Click += new System.EventHandler(this.button2_Click);
            // 
            // numUID2
            // 
            this.numUID2.Font = new System.Drawing.Font("Arial", 9F);
            this.numUID2.Location = new System.Drawing.Point(118, 20);
            this.numUID2.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numUID2.Name = "numUID2";
            this.numUID2.Size = new System.Drawing.Size(62, 21);
            this.numUID2.TabIndex = 25;
            // 
            // numDept2
            // 
            this.numDept2.Font = new System.Drawing.Font("Arial", 9F);
            this.numDept2.Location = new System.Drawing.Point(118, 148);
            this.numDept2.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numDept2.Name = "numDept2";
            this.numDept2.Size = new System.Drawing.Size(62, 21);
            this.numDept2.TabIndex = 24;
            // 
            // txtName2
            // 
            this.txtName2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtName2.Location = new System.Drawing.Point(118, 45);
            this.txtName2.Name = "txtName2";
            this.txtName2.Size = new System.Drawing.Size(219, 21);
            this.txtName2.TabIndex = 5;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(13, 151);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(75, 15);
            this.label8.TabIndex = 23;
            this.label8.Text = "Department:";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(13, 48);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(44, 15);
            this.label9.TabIndex = 6;
            this.label9.Text = "Name:";
            // 
            // txtSurname2
            // 
            this.txtSurname2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSurname2.Location = new System.Drawing.Point(118, 71);
            this.txtSurname2.Name = "txtSurname2";
            this.txtSurname2.Size = new System.Drawing.Size(219, 21);
            this.txtSurname2.TabIndex = 7;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(13, 176);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(96, 15);
            this.label10.TabIndex = 21;
            this.label10.Text = "Device privilege:";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(13, 74);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(61, 15);
            this.label11.TabIndex = 8;
            this.label11.Text = "Surname:";
            // 
            // numBranch2
            // 
            this.numBranch2.Font = new System.Drawing.Font("Arial", 9F);
            this.numBranch2.Location = new System.Drawing.Point(118, 122);
            this.numBranch2.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numBranch2.Name = "numBranch2";
            this.numBranch2.Size = new System.Drawing.Size(62, 21);
            this.numBranch2.TabIndex = 20;
            // 
            // txtPayrollNo2
            // 
            this.txtPayrollNo2.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtPayrollNo2.Location = new System.Drawing.Point(118, 97);
            this.txtPayrollNo2.Name = "txtPayrollNo2";
            this.txtPayrollNo2.Size = new System.Drawing.Size(219, 21);
            this.txtPayrollNo2.TabIndex = 9;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(13, 127);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(49, 15);
            this.label12.TabIndex = 19;
            this.label12.Text = "Branch:";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(13, 101);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 15);
            this.label13.TabIndex = 10;
            this.label13.Text = "Payroll no.:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(13, 22);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(31, 15);
            this.label14.TabIndex = 18;
            this.label14.Text = "UID:";
            // 
            // button3
            // 
            this.button3.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button3.Location = new System.Drawing.Point(378, 488);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(89, 27);
            this.button3.TabIndex = 27;
            this.button3.Text = "Update Both";
            this.button3.UseVisualStyleBackColor = true;
            this.button3.Click += new System.EventHandler(this.button3_Click);
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Arial", 9F, System.Drawing.FontStyle.Italic, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(8, 4);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(397, 45);
            this.label15.TabIndex = 28;
            this.label15.Text = "Leave UID empty to create a new record. \r\nTo update, provide a UID and new values" +
    " for fileds to be updated.\r\nTo update both employees, provide both UIDs and fiel" +
    "ds to be updated.";
            // 
            // cmbDevPriv
            // 
            this.cmbDevPriv.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDevPriv.FormattingEnabled = true;
            this.cmbDevPriv.Items.AddRange(new object[] {
            "-- select --",
            "0",
            "2"});
            this.cmbDevPriv.Location = new System.Drawing.Point(118, 173);
            this.cmbDevPriv.Name = "cmbDevPriv";
            this.cmbDevPriv.Size = new System.Drawing.Size(81, 23);
            this.cmbDevPriv.TabIndex = 26;
            // 
            // cmbDevPriv2
            // 
            this.cmbDevPriv2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cmbDevPriv2.FormattingEnabled = true;
            this.cmbDevPriv2.Items.AddRange(new object[] {
            "-- select --",
            "0",
            "2"});
            this.cmbDevPriv2.Location = new System.Drawing.Point(118, 173);
            this.cmbDevPriv2.Name = "cmbDevPriv2";
            this.cmbDevPriv2.Size = new System.Drawing.Size(81, 23);
            this.cmbDevPriv2.TabIndex = 27;
            // 
            // frmSetEmp
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(477, 524);
            this.Controls.Add(this.label15);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox1);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "frmSetEmp";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Set employee record(s)";
            this.Load += new System.EventHandler(this.frmSetEmp_Load);
            ((System.ComponentModel.ISupportInitialize)(this.numBranch)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDept)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numUID)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.numUID2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numDept2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numBranch2)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox txtName;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtSurname;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtPayrollNo;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.NumericUpDown numBranch;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numDept;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.NumericUpDown numUID;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Button button2;
        private System.Windows.Forms.NumericUpDown numUID2;
        private System.Windows.Forms.NumericUpDown numDept2;
        private System.Windows.Forms.TextBox txtName2;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.TextBox txtSurname2;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.NumericUpDown numBranch2;
        private System.Windows.Forms.TextBox txtPayrollNo2;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button3;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.ComboBox cmbDevPriv;
        private System.Windows.Forms.ComboBox cmbDevPriv2;
    }
}