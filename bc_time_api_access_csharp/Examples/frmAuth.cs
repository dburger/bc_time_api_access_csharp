﻿using BcTimeApiAccessCsharp.Gen;
using System;
using System.Windows.Forms;

namespace BcTimeApiAccessCsharp.Examples {
    public partial class frmAuth : Form {
        public frmAuth() {
            InitializeComponent();
        }
        /*For Jwt bearer, download OPEN SSL here: http://www.slproweb.com/products/Win32OpenSSL.html
            * Then use these command line steps to generate a private and public key using OpenSSL are as follows:
            Steps:
                Step 1: openssl genrsa -out privatekey.pem 1024
                Step 1: openssl req -new -x509 -key privatekey.pem -out publickey.cer -days 1825
                Step 1: openssl pkcs12 -export -out public_privatekey.pfx -inkey privatekey.pem -in publickey.cer
            Descriptions:
                Step 1: Generates a private key.
                Step 2: creates a X509 certificate (.cer file) containing your public key which you upload when registering your private application.
                Step 3: Export your x509 certificate and private key to a pfx file.
        */
        private void btnSave_Click(object sender, EventArgs e) {
            try {
                string _sGrantType = rdoJwtBearer.Checked ? OAuth2GrantType.JwtBearer.ToString() : OAuth2GrantType.ClientCredential.ToString();
                if (TokenAccess.setConfig(txtClientID.Text.Trim(), txtClientSecret.Text.Trim(), txtEncrKey.Text.Trim(), txtPkeyPath.Text.Trim(), _sGrantType)) {
                    MessageBox.Show("Configuration saved.");
                    return;
                }
                MessageBox.Show("Error: Configuration not saved.");
            } catch (Exception) {
            }
        }

        private void frmAuth_Load(object sender, EventArgs e) {
            try {
                txtClientID.Text = TokenAccess.getConfig(ConfigValue.ClientID);
                txtClientSecret.Text = TokenAccess.getConfig(ConfigValue.ClientSecret);
                txtEncrKey.Text = TokenAccess.getConfig(ConfigValue.CryptKey);
                txtPkeyPath.Text = TokenAccess.getConfig(ConfigValue.KeyPrivateFilePath);
                if (TokenAccess.getConfig(ConfigValue.GrantType) == OAuth2GrantType.JwtBearer.ToString()) {
                    rdoJwtBearer.Checked = true;
                } else {
                    rdoClientCred.Checked = true;
                }
            } catch (Exception) {
            }
        }
    }
}
