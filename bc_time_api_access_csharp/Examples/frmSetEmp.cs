﻿using BcTimeApiAccessCsharp.Gen;
using Newtonsoft.Json.Linq;
using System;
using System.Windows.Forms;

namespace BcTimeApiAccessCsharp.Examples {
    public partial class frmSetEmp : Form {
        private ApiCommHandler g_CommHandler = null;
        public frmSetEmp() {
            InitializeComponent();
            try {
                this.Cursor = Cursors.WaitCursor;
            } catch (Exception) {
            } finally {
                Cursor = Cursors.Default;
            }
        }
        private void button1_Click(object sender, EventArgs e) {
            try {
                this.Cursor = Cursors.WaitCursor;
                g_CommHandler = new ApiCommHandler();
                setOne((int)numUID.Value, txtName.Text.Trim(), txtSurname.Text.Trim(), txtPayrollNo.Text.Trim(), (int)numBranch.Value, (int)numDept.Value, cmbDevPriv.SelectedIndex == 0 ? -1 : int.Parse(cmbDevPriv.Text));
            } catch (Exception) {
            } finally {
                this.Cursor = Cursors.Default;
            }
        }

        private void button3_Click(object sender, EventArgs e) {
            try {
                this.Cursor = Cursors.WaitCursor;
                g_CommHandler = new ApiCommHandler();
                setMany();
            } catch (Exception) {
            } finally {
                this.Cursor = Cursors.Default;
            }
        }

        private void button2_Click(object sender, EventArgs e) {
            try {
                this.Cursor = Cursors.WaitCursor;
                g_CommHandler = new ApiCommHandler();
                setOne((int)numUID2.Value, txtName2.Text.Trim(), txtSurname2.Text.Trim(), txtPayrollNo2.Text.Trim(), (int)numBranch2.Value, (int)numDept2.Value, cmbDevPriv2.SelectedIndex == 0 ? -1 : int.Parse(cmbDevPriv2.Text));
            } catch (Exception) {
            } finally {
                this.Cursor = Cursors.Default;
            }
        }

        private void setOne(int _iUid, string _sName, string _sSurname, string _sPayrollNo, int _iBranch, int _iDept, int _iDevicePriv) {
            dynamic _Data = null;
            JObject _JObject = null;
            try {
                if (_iUid == 0) {
                    if (_sName == String.Empty || _sSurname == String.Empty || _sPayrollNo == String.Empty || _iBranch <= 0
                        || _iDept <= 0 || _iDevicePriv < 0) {
                        MessageBox.Show("Required fields are empty.");
                        return;
                    }
                    _JObject = new JObject()
                            {
                            { "name", _sName},
                            { "surname", _sSurname},
                            { "payroll_num", _sPayrollNo},
                            { "device_privilege", _iDevicePriv},
                            { "branch", _iBranch},
                            { "dept", _iDept},
                        };
                } else {
                    if (_sName == String.Empty && _sSurname == String.Empty && _sPayrollNo == String.Empty && _iBranch <= 0
                        && _iDept <= 0 && _iDevicePriv < 0) {
                        MessageBox.Show("You need to update at least one field.");
                        return;
                    }
                    _JObject = getUpdateObject(_iUid, _sName, _sSurname, _sPayrollNo, _iBranch, _iDept, _iDevicePriv);
                }
                if (!g_CommHandler.setData( ContentType.Emp, SetReqType.SetOne, new JArray(_JObject), out _Data )) {
                    MessageBox.Show("Request unsuccessful. Status: " + _Data.Status.ToString());
                } else {
                    if (_Data.Status == TimeApiReqStatus.Success) {
                        MessageBox.Show("Record successfully saved/updated.");
                    } else {
                        MessageBox.Show("Request unsuccessful. Status: " + _Data.Status.ToString() + ".");
                    }
                }
            } catch (Exception) {
            }
        }

        private void setMany() {
            try {
                dynamic _Data = null;
                JArray _JArray = new JArray();
                _JArray.Add(getUpdateObject((int)numUID.Value, txtName.Text.Trim(), txtSurname.Text.Trim(), txtPayrollNo.Text.Trim(), (int)numBranch.Value, (int)numDept.Value, cmbDevPriv.SelectedIndex == 0 ? -1 : int.Parse(cmbDevPriv.Text)));
                _JArray.Add(getUpdateObject((int)numUID2.Value, txtName2.Text.Trim(), txtSurname2.Text.Trim(), txtPayrollNo2.Text.Trim(), (int)numBranch2.Value, (int)numDept2.Value, cmbDevPriv2.SelectedIndex == 0 ? -1 : int.Parse(cmbDevPriv2.Text)));
                if (!g_CommHandler.setData( ContentType.Emp, SetReqType.SetMany, _JArray, out _Data )) {
                    MessageBox.Show("Request unsuccessful. Status: " + _Data.Status.ToString());
                } else {
                    if (_Data.Status == TimeApiReqStatus.Success) {
                        MessageBox.Show("Records successfully saved/updated.");
                    } else {
                        MessageBox.Show("Request unsuccessful. Status: " + _Data.Status.ToString() + ".");
                    }
                }
            } catch (Exception) {
            }
        }

        private JObject getUpdateObject(int _iUid, string _sName, string _sSurname, string _sPayrollNo, int _iBranch, int _iDept, int _iDevPriv) {
            try {
                JObject _JObject = new JObject() {
                { "cont_uid", _iUid }
                };
                if (_sName != String.Empty) {
                    _JObject["name"] = _sName;
                }
                if (_sSurname != String.Empty) {
                    _JObject["surname"] = _sSurname;
                }
                if (_sPayrollNo != String.Empty) {
                    _JObject["payroll_num"] = _sPayrollNo;
                }
                if (_iDevPriv > -1) {
                    _JObject["device_privilege"] = _iDevPriv;
                }
                if (_iBranch > 0) {
                    _JObject["branch"] = _iBranch;
                }
                if (_iDept > 0) {
                    _JObject["dept"] = _iDept;
                }
                return _JObject;
            } catch (Exception) {
            }
            return null;
        }

        private void frmSetEmp_Load(object sender, EventArgs e) {
            cmbDevPriv.SelectedIndex = 0;
            cmbDevPriv2.SelectedIndex = 0;
        }
    }
}
