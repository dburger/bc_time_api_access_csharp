﻿using BcTimeApiAccessCsharp.Gen;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace BcTimeApiAccessCsharp.Examples {
    public partial class frmMain : Form {
        public frmMain() {
            InitializeComponent();
        }

        private void ExitToolsStripMenuItem_Click(object sender, EventArgs e) {
            Application.Exit();
        }

        private void editMenu_Click(object sender, EventArgs e) {
            try {
                this.Cursor = Cursors.WaitCursor;
                if (!TokenAccess.deterToken()) {
                    MessageBox.Show("Could not get/generate an access token.");
                    return;
                }
                Clipboard.SetText( TokenAccess.Token);
                MessageBox.Show("The access token has been copied to the clipboard.\n\nIt expires at " + TokenAccess.TokenExpire.ToString() + ".");
            } catch (Exception) {
                //Exception logic
            } finally {
                this.Cursor = Cursors.Default;
            }
        }

        private void viewMenu_Click(object sender, EventArgs e) {
            this.showForm(new frmGetEmp());
        }

        private void toolsMenu_Click(object sender, EventArgs e) {
            this.showForm(new frmSetEmp());
        }

        private void setConfigValuesToolStripMenuItem_Click(object sender, EventArgs e) {
            this.showForm(new frmAuth());
        }

        private void showForm(Form _frm) {
            try {
                _frm.MdiParent = this;
                _frm.Show();
            } catch (Exception) {
            }
        }
    }
}
