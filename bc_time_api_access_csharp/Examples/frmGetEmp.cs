﻿using BcTimeApiAccessCsharp.Gen;
using System;
using System.Data;
using System.Windows.Forms;

namespace BcTimeApiAccessCsharp.Examples {
    public partial class frmGetEmp : Form {
        public frmGetEmp() {
            InitializeComponent();
        }

        private void button1_Click(object sender, EventArgs e) {
            displayEmp();
        }

        private void displayEmp() {
            try {
                Cursor = Cursors.WaitCursor;
                DataSet _Ds = getEmpDataset(/*_iPage=*/1, /*_iRowCount=*/20, getEmpIds());
                if (_Ds != null) {
                    dgvEmp.AutoGenerateColumns = false;
                    dgvEmp.DataSource = _Ds.Tables[0];
                    dgvEmp.Columns["uid"].DataPropertyName = "uid";
                    dgvEmp.Columns["name"].DataPropertyName = "name";
                    dgvEmp.Columns["surname"].DataPropertyName = "surname";
                    dgvEmp.Columns["payroll_num"].DataPropertyName = "payroll_num";
                } else {
                    MessageBox.Show("Data not received.", "No data", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                }
            } catch (Exception) {
            } finally {
                Cursor = Cursors.Default;
            }
        }

        private uint[] getEmpIds() {
            try {
                if (textBox1.Text.Trim() == String.Empty) {
                    return null;
                }
                string[] _sEmps = textBox1.Text.Trim().Replace(" ", "").Split(new char[] { ',' });
                return Array.ConvertAll(_sEmps, _sEmp => uint.Parse(_sEmp));
            } catch (Exception) {
            }
            return null;
        }
        private DataSet getEmpDataset(int _iPageCount, int _iRowCount, uint[] _iEmpIDs) {
            try {
                ApiCommHandler _CommHandler = new ApiCommHandler();
                DataSet _Ds = new DataSet();
                DataTable _DataTable;
                _DataTable = new DataTable();
                _DataTable.Columns.Add("uid");
                _DataTable.Columns.Add("name");
                _DataTable.Columns.Add("surname");
                _DataTable.Columns.Add("payroll_num");
                int _iCurrentPage = 1;
                dynamic _Data = null;
                dynamic _DataStr;
                while (_iPageCount >= _iCurrentPage) {
                    if (!_CommHandler.getData(ContentType.Emp, out _Data, _iPage: _iCurrentPage, _iRowCount: _iRowCount, _iContUids: _iEmpIDs)) {
                        break;
                    }
                    _DataStr = _Data.Data;
                    if (_Data == null || _DataStr == null) {
                        break;
                    }
                    int _iCount = 0;
                    if (_DataStr.Count == null) {
                        _iCount = 1;
                    } else {
                        _iCount = _DataStr.Count;
                    }
                    for (int _iI = 0; _iI < _iCount; _iI++) {
                        if (_iCount == 1) {
                            _DataTable.Rows.Add(
                                _DataStr["uid"],
                                _DataStr["name"],
                                _DataStr["surname"],
                                _DataStr["payroll_num"]);
                        } else {
                            _DataTable.Rows.Add(
                                _DataStr[_iI]["uid"],
                                _DataStr[_iI]["name"],
                                _DataStr[_iI]["surname"],
                                _DataStr[_iI]["payroll_num"]);
                        }
                    }
                    if (_DataStr.Count == 0) {
                        break;
                    }
                    _iCurrentPage++;
                }
                if (_Ds.Tables.Count != 0) {
                    _Ds.Tables.Clear();
                }
                if (_DataTable.Rows.Count == 0) {
                    return null;
                }
                _Ds.Tables.Add(_DataTable);
                return _Ds;
            } catch (Exception) {
            }
            return null;
        }
    }
}
